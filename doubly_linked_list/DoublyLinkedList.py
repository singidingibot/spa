from node.Node import Node

class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.size = 0

    def is_not_empty(self):
        if self.size > 0:
            return True
        raise IndexError("List is empty")

    def print_list(self):
        cur = self.head
        while cur:
            print(cur.data)
            cur = cur.next

    def append(self, data):
        if self.size == 0:
            new_node = Node(data)
            new_node.prev = None
            self.head = new_node
        else:
            new_node = Node(data)
            cur = self.head
            while cur.next:
                cur = cur.next
            cur.next = new_node
            new_node.prev = cur
            new_node.next = None
        self.size += 1

    def prepand(self, data):
        new_node = Node(data)
        if self.size == 0:
            self.append(data)
        else:
            new_node = Node(data)
            self.head.prev = new_node
            new_node.next = self.head
            self.head = new_node
            new_node.prev = None
        self.size += 1

    def first(self):
        if self.is_not_empty():
            return self.head.data

    def last(self):
        if self.is_not_empty():
            cur = self.head
            while cur.next:
                cur = cur.next
            return cur.data

    def __getitem__(self, key):
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for element in self:
                if counter == key:
                    return element
                counter += 1
        else:
            raise IndexError()

    def __iter__(self):
        cur = self.head
        while cur:
            yield cur.data
            cur = cur.next

    def __len__(self):
        if self.is_not_empty():
            return self.size