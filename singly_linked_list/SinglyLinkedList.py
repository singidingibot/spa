from node.Node import Node


class SinglyLinkedList:
    def __init__(self):
        self.head = None
        self.size = 0

    def is_not_empty(self):
        if self.size > 0:
            return True
        raise IndexError("List is empty!")

    def print_list(self):
        current_node = self.head
        while current_node:
            print(current_node.data)
            current_node = current_node.next

    def append(self, data):
        new_node = Node(data)
        if self.size == 0:
            self.head = new_node
        else:
            current_node = self.head
            while current_node.next:
                current_node = current_node.next
            current_node.next = new_node
        self.size += 1

    def prepand(self, data):
        new_node = Node(data)
        if self.size == 0:
            self.head = new_node
        else:
            new_node.next = self.head
            self.head = new_node
        self.size += 1

    def first(self):
        if self.is_not_empty():
            return self.head.data

    def last(self):
        if self.is_not_empty():
            curr = self.head
            while curr.next:
                curr = curr.next
            return curr.data

    def remove_first(self):
        if self.is_not_empty():
            self.head = self.head.next
        self.size -= 1

    def remove_last(self, data):
        if self.size == 0:
            print("List is empty")
            return
        elif self.size == 1:
            self.head = None
        else:
            current_node = self.head
            while current_node.next:
                current_node = current_node.next
            current_node = None
        self.size -= 1

    def __getitem__(self, key):
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for element in self:
                if counter == key:
                    return element
                counter += 1
        else:
            raise IndexError()

    def __setitem__(self, key):
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for node in self._iter_node():
                if counter == key:
                    node.value = value
                    break
                counter += 1
        else:
            raise IndexError() 

    def __iter__(self):
        cur = self.head
        while cur:
            yield cur.data
            cur = cur.next

    def __len__(self):
        if self.is_not_empty():
            return self.size
