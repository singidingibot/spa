class Magacin:
    def __init__(self, tip, kapacitet, zauzetost):
        self.tip = tip
        self.kapacitet = kapacitet
        self.zauzetost = zauzetost

    def __eq__(self, value):
        return self.kapacitet == value.kapacitet

    def __gt__(self, value):
        return self.__eq__ and self.zauzetost < value.zauzetost

    def __ge__(self, value):
        return self.__eq__ and self.zauzetost <= value.zauzetost

    @staticmethod
    def najveci_magacin(dll):
        if len(dll) == 0:
            print('List is empty!')
            return
        else:
            najveci = dll.first()
            for magacin in dll:
                if magacin > najveci:
                    magacin = najveci
                return print(f'{najveci.tip} {najveci.zauzetost} {najveci.kapacitet}')

    

"""
---------------------------------------------------------------------------
"""

from doubly_linked_list.DoublyLinkedList import DoublyLinkedList

magacini = DoublyLinkedList()

magacini.append(Magacin("Vojni", 200, 100))
magacini.append(Magacin("Poljoprivredni", 50, 10))
magacini.append(Magacin("Idea", 80, 30))
magacini.prepand(Magacin("Warehouse", 300, 259))

#Magacin.najveci_magacin(magacini)

for magacin in magacini:
    print(magacin.tip)