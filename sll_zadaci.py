class Automobil:
    def __init__(self, marka, model, maks_brzina):
        self.marka = marka
        self.model = model
        self.maks_brzina = maks_brzina
    
    def __eq__(self, value):
        return self.maks_brzina == value.maks_brzina

    def __ge__(self, value):
        return self.maks_brzina == value.maks_brzina

    def __gt__(self, value):
        return self.maks_brzina > value.maks_brzina

    def __ne__(self, value):
        return self.maks_brzina != value.maks_brzina

"""
---------------------------------------------------------------------------
"""
from singly_linked_list.SinglyLinkedList import SinglyLinkedList

sll = SinglyLinkedList()
sll.append(Automobil("BMW", "E39", 220))
sll.append(Automobil("Mercedes-Benz", "E220", 210))
sll.append(Automobil("Audi", "A6", 200))

def najbrzi_automobil():
    if len(sll) < 1:
        return
    else:
        najbrzi_automobil = sll.first()
        for auto in sll:
            if auto > najbrzi_automobil:
                auto = najbrzi_automobil
            return print(najbrzi_automobil.marka, najbrzi_automobil.maks_brzina)

def rm():
    if len(sll) < 1:
        return
    else:
        najbrzi = sll.first()
        for auto in sll:
            if auto > najbrzi:
                auto = najbrzi
            if auto != najbrzi:
                sll.remove()
                return
        



# najbrzi_automobil()
rm()