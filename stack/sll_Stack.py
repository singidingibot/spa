from singly_linked_list.SinglyLinkedList import SinglyLinkedList

class Stack:
    def __init__(self):
        self.sll = SinglyLinkedList()

    def push(self, data):
        return self.sll.append(data)

    def pop(self):
        return self.sll.remove_first()

    def top(self):
        return self.sll.last()

    def __len__(self):
        return len(self.sll)