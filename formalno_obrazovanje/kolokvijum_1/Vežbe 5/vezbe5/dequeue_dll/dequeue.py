from dll.dll import DoublyLinkedList


class Dequeue:
    def __init__(self):
        """
        Realizacije deka na osnovu dvostruko povezane liste.
        """
        self.lista = DoublyLinkedList()

    def add_first(self, value):
        """
        Metoda kojom se dodaje na pocetak deka.
        :param value: vrednost koja se dodaje
        """
        self.lista.prepend(value)

    def add_last(self, value):
        """
        Metoda kojom se dodaje na kraj deka.
        :param value: vrednost koja se dodaje
        """
        self.lista.append(value)

    def remove_first(self):
        """
        Metoda kojom se uklanja sa pocetka deka.
        :returns: vrednost sa pocetka
        :raises IndexError: ako je dek prazan
        """
        try:
            return self.lista.remove_first()
        except IndexError:
            raise IndexError("Dek je prazan!")

    def remove_last(self):
        """
        Metoda kojom se uklanja sa kraja deka.
        :returns: vrednost sa kraja
        :raises IndexError: ako je dek prazan
        """
        try:
            return self.lista.remove_last()
        except IndexError:
            raise IndexError("Dek je prazan!")

    def first(self):
        """
        Pristupanje prvom elementu deka.
        :returns: vrednost sa pocetka
        :raises IndexError: ako je dek prazan
        """
        try:
            return self.lista.first()
        except IndexError:
            raise IndexError("Dek je prazan")

    def last(self):
        """
        Pristupanje poslednjem elementu deka.
        :returns: vrednost sa kraja
        :raises IndexError: ako je dek prazan
        """
        try:
            return self.lista.last()
        except IndexError:
            raise IndexError("Dek je prazan!")

    def __len__(self):
        """
        Redefinisana metoda koja vraca duzinu strukture.
        """
        return len(self.lista)
    