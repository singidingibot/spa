from dll.dll import DoublyLinkedList


class Queue:
    def __init__(self):
        """
        Realizacija reda na osnovu dvostruko povezane liste.
        """
        self.lista = DoublyLinkedList()

    def enqueue(self, value):
        """
        Metoda koja dodaje element u red. Dodavanje se vrsi na kraj.
        :param value: vrednost koja se dodaje
        """
        self.lista.append(value)

    def dequeue(self):
        """
        Metoda koja dodaje element u red. Dodavanje se vrsi na kraj.
        :param value: vrednost koja se dodaje
        :returns: vrednost sa pocetka reda
        :raises IndexError: ako je red prazan
        """
        try:
            return self.lista.remove_first()
        except IndexError:
            raise IndexError("Red je prazan!")

    def first(self):
        """
        Metoda koja pristupa prvom elementu u redu.
        :returns: vrednost sa pocetka reda
        :raises IndexError: ako je red prazan
        """
        try:
            return self.lista.first()
        except IndexError:
            raise IndexError("Red je prazan!")

    def __len__(self):
        """
        Redefinisana metoda koja vraca duzinu strukture.
        """
        return len(self.lista)