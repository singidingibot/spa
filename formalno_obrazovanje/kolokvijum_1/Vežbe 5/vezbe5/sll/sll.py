from sll.node import Node

class SinglyLinkedList:
    """
    Klasa koja predstavlja jednostruko povezanu listu.
    """
    def __init__(self):
        """
        Inicijalizator jednostruko povezane liste.
        Lista ima pocetak (head), kraj (tail) i velicinu.
        """
        self.head = None # prazna lista nema pocetni cvor
        self.tail = None
        self.size = 0

    def append(self, value):
        """
        Metoda koja dodaje vrednost na kraj jednostruko povezane liste.
        :param value: vrednost koju dodajemo u listu, najpre se smesta u cvor
        """
        new_node = Node(value)
        # kada je lista prazna
        if self.size == 0:
            self.head = new_node
            self.tail = new_node
        # kada imamo samo jedan cvor
        elif self.head == self.tail: # ili if self.size == 1:
            self.head.pointer = new_node
            self.tail = new_node
        # kada imamo dva ili vise cvorova
        else:
            self.tail.pointer = new_node
            self.tail = new_node
        self.size += 1

    def prepend(self, value):
        """
        Metoda koja dodaje vrednost na pocetak jednostruko povezane liste.
        :param value: vrednost koju dodajemo u listu, najpre se smesta u cvor
        """
        new_node = Node(value)
        # kada je lista prazna
        if self.size == 0:
            self.head = new_node
            self.tail = new_node
        else:
            new_node.pointer = self.head
            self.head = new_node
        self.size += 1

    def remove_first(self):
        """
        Metoda koja uklanja cvor sa pocetka jednostruko povezane liste.
        """
        # ako je lista prazna
        value = None
        if self.size == 0:
            raise IndexError("Lista je prazna!")
        # ako ima samo jedan element
        elif self.head == self.tail:
            value = self.head.value
            self.head = None
            self.tail = None
        else:
            value = self.head.value
            self.head = self.head.pointer
        self.size -= 1
        return value

    def remove_last(self):
        """
        Metoda koja uklanja cvor sa kraja jednostruko povezane liste.
        """
        value = None
        # ako je lista prazna
        if self.size == 0:
            raise IndexError("Lista je prazna!")
        # ako ima samo jedan element
        elif self.head == self.tail:
            value = self.head.value
            self.head = None
            self.tail = None
        else:
            current = self.head
            while current.pointer != self.tail:
                current = current.pointer
            value = current.pointer.value
            current.pointer = None
            self.tail = current
        self.size -= 1
        return value

    def first(self):
        """
        Metoda koja dobavlja vrednost na pocetnom cvoru jednostruko povezane liste.
        """
        if self.size > 0:
            return self.head.value
        else:
            raise IndexError("Lista je prazna!")

    def last(self):
        """
        Metoda koja dobavlja vrednost na kranjem cvoru jednostruko povezane liste.
        """
        if self.size > 0:
            return self.tail.value
        else:
            raise IndexError("Lista je prazna!")

    def __iter__(self):
        """
        Iterator koji omogucava generisanje elemenat koji se nalaze u jednostruko
        povezanoj listi. Iterator nam je potreban ako zelimo da for petljom prolazimo
        kroz jednostruko povezanu listu.
        """
        current = self.head
        while current is not None:
            yield current.value
            current = current.pointer

    def __len__(self):
        """
        Redefinisani operator koji vraca duzinu strukture.
        """
        return self.size