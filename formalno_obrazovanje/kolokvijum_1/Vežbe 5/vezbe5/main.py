from stack_dll.stack import Stack
from queue_dll.queue import Queue
from dequeue_dll.dequeue import Dequeue as Deqdll
from dequeue_sll.dequeue import Dequeue as Deqsll
from sll.sll import SinglyLinkedList

# TODO: Pogledati izmene uvedene u Dvostruko i jednostruko povezanu listu

# lista = SinglyLinkedList()

# lista.append(10)
# lista.append(12)
# print(lista.remove_last())

# stek = Stack()

# stek.push("crvena")
# stek.push("plava")
# stek.push("zelena")
# stek.push("zuta")


# print(stek.top())

# stek2 = Stack()

# for _ in range(len(stek)):
#     stek2.push(stek.pop())

# print(stek2.top())  # zuta, zelena, crvena, plava

# try:
#     stek.pop()
# except IndexError:
#     print("Nije moguce ukloniti element iz praznog steka.")

# print("Kraj programa.")


# red = Queue()

# for i in range(10):
#     red.enqueue(i)

# for _ in range(len(red)):
#     print(red.dequeue())