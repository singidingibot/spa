class Node:
    """
    Klasa koja predstavlja cvor liste.
    Cvor sadrzi vrednost i pokazivac na sledeci element.
    """
    def __init__(self, value, pointer=None):
        """
        Inicijalizator cvora.
        :param value: vrednost na cvoru
        :param pointer: pokazivac na sledeci cvor
        """
        self.value = value
        self.pointer = pointer