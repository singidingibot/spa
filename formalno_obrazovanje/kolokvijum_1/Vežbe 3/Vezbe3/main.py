# putanje su relativne u odnosu na mesto pokretanja i mesto glavne datoteke
from sll.sll import SinglyLinkedList
from automobil import Automobil


lista = SinglyLinkedList()

for i in range(2):
    lista.append(i)

# for i in range(10, 4, -1):
#     lista.prepend(i)

lista.remove_last()

for element in lista:
    print(element)

print(len(lista))

a1 = Automobil("VW", "Golf 5", 230)
a2 = Automobil("VW", "Golf 4", 250)
a3 = Automobil("VW", "Golf 3", 180)

lista_automobila = SinglyLinkedList()
lista_automobila.append(a1)
lista_automobila.prepend(a2)
lista_automobila.append(a3)

def najbrzi_auto():
    if len(lista_automobila) < 1:
        return
    else:
        najbrzi_auto = lista_automobila.first()
    for auto in lista_automobila:
        if auto > najbrzi_auto:
            najbrzi_auto = auto
    print(najbrzi_auto.marka, najbrzi_auto.model)

najbrzi_auto()