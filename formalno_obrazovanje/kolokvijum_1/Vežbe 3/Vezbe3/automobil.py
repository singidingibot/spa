class Automobil:
    """
    Klasa koja predstavlja automobil.
    Omogucava nam da poredimo automobile spram vrednosti maksimalne brzine.
    """
    def __init__(self, marka, model, maks_brzina):
        """
        Inicijalizator klase Automobil.
        :param marka: marka automobila
        :param model: model automobila
        :param maks_brzina: maksimalna brzina automobila
        """
        self.marka = marka
        self.model = model
        self.maks_brzina = maks_brzina
    
    # Redefinisanje operatora, dovoljno nam je da redefinisemo ==, >, >= ili
    # ==, <, <=, ostali se dobijaju negacijom definisanih.
    def __eq__(self, other):
        """
        Operator jednakosti - dva automobila su ista ako su im iste brzine
        """
        return self.maks_brzina == other.maks_brzina

    def __ge__(self, other):
        """
        Operator vece ili jednako - automobil je veci ili jednak drugom, ako mu je
        brzina veca ili jednaka drugom automobilu.
        """
        return self.maks_brzina >= other.maks_brzina

    def __gt__(self, other):
        """
        Operator strogo vece - automobil je veci od drugog, ako mu je
        brzina veca od drugog automobila.
        """
        return self.maks_brzina > other.maks_brzina