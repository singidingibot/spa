from prvi_paket.prva_klasa import PrvaKlasa
from prvi_paket.osoba import Osoba

# Demonstracija redosleda izvršavanja koda
# i pristupa metodama i atributima objekta
print("Pocetak skripte")
prva_instanca = PrvaKlasa("Vrednost")
print(prva_instanca.prvi_atribut)
prva_instanca.prvi_atribut = 10
print(prva_instanca.prvi_atribut)

lista = []
# Rešenje Zadatka 1
def najveci_broj(lista):
    # ako je lista prazna, nema najveceg
    if len(lista) == 0:
        return
    # kada ima samo jedan element, onda je taj najveci
    elif len(lista) == 1:
        return lista[0]
    # u suprotnom, trazimo najveci, i pri tome prvi postavljamo kao
    # trenutno najveci
    najveci = lista[0]
    for i in range(1, len(lista)):
        if lista[i] > najveci:
            najveci = lista[i]
    return najveci

print("Najveci broj u listi", lista, "je broj", najveci_broj(lista))

# Resenje Zadatka 2
lista_osoba = []
lista_osoba.append(Osoba("Marko", "Markovic", 10))
lista_osoba.append(Osoba("Janko", "Jankovic", 26))
lista_osoba.append(Osoba("Milena", "Milenic", 37))
lista_osoba.append(Osoba("Zarko", "Zarkovic", 91))
lista_osoba.append(Osoba("Petar", "Petrovic", 4))

# Najstariju i najmladju osobu trazimo preko funkcija
# algoritam slican funkciji najveci_broj
def najstarija_osoba(lista):
    if len(lista) == 0:
        return
    elif len(lista) == 1:
        return lista[0]
    najveci = lista[0]
    for i in range(1, len(lista)):
        if lista[i].starost > najveci.starost:
            najveci = lista[i]
    return "Najstarija osoba je {} {}".format(najveci.ime, najveci.prezime)

def najmladja_osoba(lista):
    if len(lista) == 0:
        return
    elif len(lista) == 1:
        return lista[0]
    najmanji = lista[0]
    for i in range(1, len(lista)):
        if lista[i].starost < najmanji.starost:
            najmanji = lista[i]
    return "Najmladja osoba je {} {}".format(najmanji.ime, najmanji.prezime)

print(najstarija_osoba(lista_osoba))
print(najmladja_osoba(lista_osoba))