class Magacin:
    """
    Klasa koja predstavlja magacin
    """
    def __init__(self, tip, kapacitet, zauzetost):
        """
        Inicijalizator Magacina.
        :param tip: tip magacina
        :param kapacitet: kapacitet magacina
        :param zauzetost: zauzetost magacina
        """
        self.tip = tip
        self.kapacitet = kapacitet
        self.zauzetost = zauzetost

    def __eq__(self, other):
        """
        Redefinisani operator ==
        """
        return self.kapacitet == other.kapacitet

    def __gt__(self, other):
        """
        Redefinisani operator >
        """
        return self.kapacitet == other.kapacitet and self.zauzetost < other.zauzetost

    def __ge__(self, other):
        """
        Redefinisani operator >=
        """
        return self.kapacitet == other.kapacitet and self.zauzetost <= other.zauzetost

    def __str__(self):
        """
        Redefinisanje magicne metode str, koja vraca reprezentaciju objekta kao tip string.
        Korisno je definisati metodu, kako bi ostale funkcije koje zahtevaju string reprezentaciju
        objekta (poput print funkcije) mogle da funkcionisu.
        """
        return "Magacin >> tip: {tip}; kapacitet: {kapacitet}; zauzetost: {zauzetost}".format(
            tip=self.tip, kapacitet=self.kapacitet, zauzetost=self.zauzetost
        )