from dll.node import Node

class DoublyLinkedList:
    """
    Klasa koja predstavlja dvostruko povezanu listu.
    """
    def __init__(self):
        """
        Inicijalizator dvostruko povezane liste, koja je inicijalno prazna.
        """
        self.head = Node(None) # sentinel cvor pocetka
        self.tail = Node(None) # sentinel cvor kraja

        # uvezivanje cvorova
        self.head.next_p = self.tail
        self.tail.prev_p = self.head

        self.size = 0 # definisanje velicine liste da je 0

    def append(self, value):
        """
        Metoda koja dodaje vrednost value u cvor koji se smesta na kraj strukture.
        :param value: vrednost koju zelimo da dodamo na kraj
        """
        new_node = Node(value, self.tail.prev_p, self.tail)

        self.tail.prev_p.next_p = new_node
        self.tail.prev_p = new_node

        self.size += 1

    def prepend(self, value):
        """
        Metoda koja dodaje vrednost value u cvor koji se smesta na pocetak strukture.
        :param value: vrednost koju zelimo da dodamo na pocetak
        """
        new_node = Node(value, self.head, self.head.next_p)

        self.head.next_p.prev_p = new_node
        self.head.next_p = new_node

        self.size += 1

    def remove_first(self):
        """
        Metoda koja uklanja cvor sa pocetka strukture.
        """
        if self.size == 0:
            return
        value = self.head.next_p.value
        self.head.next_p = self.head.next_p.next_p
        self.head.next_p.prev_p = self.head
        self.size -= 1
        return value

    def remove_last(self):
        """
        Metoda koja uklanja cvor sa kraja strukture.
        """
        if self.size == 0:
            return
        value = self.tail.prev_p.value
        self.tail.prev_p.prev_p.next_p = self.tail
        self.tail.prev_p = self.tail.prev_p.prev_p

        self.size -= 1
        return value

    def first(self):
        """
        Metoda koja vraca vrednost smestenu u prvi cvor strukture.
        :return: vrednost prvog elementa
        """
        if self.size == 0:
            return # return None
        return self.head.next_p.value

    def last(self):
        """
        Metoda koja vraca vrednost smestenu u poslednji cvor strukture.
        :returns: vrednost poslednjeg elementa
        """
        if self.size == 0:
            return
        return self.tail.prev_p.value

    def __iter__(self):
        """
        Magicna metoda koja vraca iterator vrednosti na cvorovima.
        """
        current = self.head.next_p
        while current != self.tail:
            yield current.value
            current = current.next_p

    def _iter_node(self):
        """
        Privatna metoda (pocinje sa _) koja vraca iterator cvorova.
        """
        current = self.head.next_p
        while current != self.tail:
            yield current
            current = current.next_p

    def __len__(self):
        """
        Magicna metoda koja redefinise ponasanje ugradjene funkcije len, nad ovom strukturom.
        """
        return self.size

    def __str__(self):
        """
        Magicna metoda koja vraca reprezentaciju objekta kao stringa.
        Vracena reprezentacija je bas ona poput klasicne Python liste: [1, 2, 3], gde su 1,2,3 vrednosti na
        cvorovima nase dvostruko povezane liste.
        """
        ret_val = "["
        for i in range(len(self)):
            ret_val += str(self[i])
            if i < len(self) - 1:
                ret_val += ", "
        return ret_val + "]"

    def __getitem__(self, key):
        """
        Magicna metoda koja omogucava indeksiranje strukture. Sada je moguce da dobavimo vrednost
        na prosledjenoj poziciji preko: lista[key], gde je key indeks (pozicija) elementa kojeg
        zelimo da dobavimo.
        :param key: kljuc za indeksiranje
        :type key: int
        :returns: vrednost na datom indeksu
        :raises: :class `TypeError`: ako prosledjeni kljuc nije tipa int
        :raises: :class `IndexError`: ako je indeks van opsega strukture
        """
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for element in self:
                if counter == key:
                    return element
                counter += 1
        else:
            raise IndexError()
    
    def __setitem__(self, key, value):
        """
        Magicna metoda koja omogucava promenu sadrzaja strukture na zadatom indeksu.
        Sada je moguce da koristimo sledeci zapis lista[key] = value
        :param key: kljuc za indeksiranje
        :type key: int
        :param value: nova vrednost koja se postavlja
        :returns: vrednost na datom indeksu
        :raises: :class `TypeError`: ako prosledjeni kljuc nije tipa int
        :raises: :class `IndexError`: ako je indeks van opsega strukture
        """
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for node in self._iter_node():
                if counter == key:
                    node.value = value
                    break
                counter += 1
        else:
            raise IndexError()

    def sort(self):
        """
        Sortiranje liste uz pomoc selection sort algoritma.
        """
        for i in range(len(self)-1):
            j_min = i
            for j in range(i+1, len(self)):
                if self[j] < self[j_min]:
                    j_min = j
            if i != j_min:
                # zameni ta dva elementa (vrednosti na cvoru)
                self[i], self[j_min] = self[j_min], self[i]


    # FIXME: ova metoda je samo dodata zbog zadatka, inace ne treba da postoji u strukturi
    @staticmethod
    def najveci_magacin(lista_magacina):
        """
        Metoda koja pronalazi najveci magacin iz liste magacina.
        :param lista_magacina: dvostruko povezna lista sa vrednostima tipa Magacin
        :retruns: None ako je lista prazna ili najveci Magacin
        """
        if len(lista_magacina) == 0:
            return
        najveci = lista_magacina.first()
        for magacin in lista_magacina:
            if magacin > najveci:
                najveci = magacin

        return najveci
    
