from dll.dll import DoublyLinkedList
from magacin import Magacin

lista = DoublyLinkedList()

for i in range(5):
    lista.append(i)

for i in range(5):
    lista.prepend(i)

for i in range(3):
    lista.remove_first()

for element in lista:
    print(element)

print("Prvi", lista.first())
print("Poslednji", lista.last())

# Zadaci
lista_magacina = DoublyLinkedList()
lista_magacina.append(Magacin("vojni", 200, 150))
lista_magacina.append(Magacin("carinski", 100, 40))
lista_magacina.append(Magacin("robnih rezervi", 200, 25))
lista_magacina.append(Magacin("vojni", 30, 163))
lista_magacina.append(Magacin("vojni", 150, 50))

najveci = DoublyLinkedList.najveci_magacin(lista_magacina)
def napuni_najveci(najveci, lista_magacina):
    """
    Funkcija za prebacivanje sadrzaja iz ostalih magacina u najveci.
    Prebacivanje se vrsi dok se kapacitet najveceg ne popuni.
    :param najveci: najveci magacin
    :param lista_magacina: dvostruko povezana lista sa elementima tipa Magacin
    """
    for magacin in lista_magacina:
        if magacin is not najveci: # ovde koristimo poredjenje identifikatora, zasto?
            slobodno_mesta = najveci.kapacitet - najveci.zauzetost
            if slobodno_mesta > 0: # znaci da smo najveci popunili, pa ne prolazimo kroz ostatak liste
                if magacin.zauzetost > 0 and magacin.zauzetost <= slobodno_mesta:
                    za_premestanje = magacin.zauzetost
                else:
                    za_premestanje = slobodno_mesta
                najveci.zauzetost +=  za_premestanje
                magacin.zauzetost -= za_premestanje
            else:
                break


napuni_najveci(najveci, lista_magacina)
print(lista_magacina)

lista_magacina.sort()

# Iterisanje preko indeksiranja (__getitem__)
for i in range(len(lista_magacina)):
    print(lista_magacina[i])