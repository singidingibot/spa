from tacka2d import Tacka2D


class Tacka3D(Tacka2D):
    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z

    def ispisi_koordinate(self):
        print("X je: {}, a Y je: {}, a Z je: {}".format(self.x, self.y, self.z))