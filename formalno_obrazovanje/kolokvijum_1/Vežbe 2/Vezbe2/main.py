from tacka2d import Tacka2D
from tacka3d import Tacka3D
from osoba import Osoba
from student import Student
from korisnik import Korisnik
from administrator import Administrator

t2d = Tacka2D(10, 20)
t3d = Tacka3D(5, 10, 15)

t2d.ispisi_koordinate()
t3d.ispisi_koordinate()

# Ne mozemo instancirati apstraktnu klasu (sa apstraktnim metodama)
# osoba1 = Osoba("Marko", "Markovic")
# print(osoba1.ime, osoba1.prezime)

# lista_studenata = []
# lista_studenata.append(Student("Marko", "Markovic", "SII", "2018/270331", 8.7))
# lista_studenata.append(Student("Janko", "Jankovic", "SII", "2018/270332", 9.3))
# lista_studenata.append(Student("Petar", "Petrovic", "IT", "2018/270031", 9.1))
# lista_studenata.append(Student("Dusan", "Dusanovic", "IT", "2018/270321", 8.1))

# print(Student.najbolji_student(lista_studenata))

# korisnik = Korisnik("korisnik", "korisnik")
# administrator = Administrator("admin", "admin", "123")

# # korisnik.uloguj_se()
# administrator.uloguj_se()