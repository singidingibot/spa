from abc import ABC, abstractmethod


class Osoba(ABC):
    @abstractmethod
    def __init__(self, ime, prezime):
        self.ime = ime
        self.prezime = prezime

    