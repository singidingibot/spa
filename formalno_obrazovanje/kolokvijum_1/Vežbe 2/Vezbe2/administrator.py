from korisnik import Korisnik
from getpass import getpass


class Administrator(Korisnik):
    def __init__(self, korisnicko_ime, lozinka, sigurnosni_kod):
        super().__init__(korisnicko_ime, lozinka)
        self.sigurnosni_kod = sigurnosni_kod

    def uloguj_se(self):
        unos_korisnicko_ime = input("Unesite korisnicko ime: ")
        unos_lozinka = getpass("Unesite lozinku: ")
        unos_sigurnosni_kod = getpass("Unesite i sigurnosni kod: ")
        if unos_korisnicko_ime == self.korisnicko_ime and unos_lozinka == self.lozinka and unos_sigurnosni_kod == self.sigurnosni_kod:
            print("Uspesno logovanje!")
            return True
        else:
            print("Netacni podaci")
            return False