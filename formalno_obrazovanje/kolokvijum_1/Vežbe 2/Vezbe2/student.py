from osoba import Osoba


class Student(Osoba):
    def __init__(self, ime, prezime, smer, broj_indeksa, prosecna_ocena):
        super().__init__(ime, prezime)
        self.smer = smer
        self.broj_indeksa = broj_indeksa
        self.prosecna_ocena = prosecna_ocena

    @staticmethod
    def najbolji_student(lista_studenata):
        if len(lista_studenata) == 0:
            return "Nema najboljeg"
        elif len(lista_studenata) == 1:
            return "Najbolji je: {} {} sa indeksom: {}".format(lista_studenata[0].ime,
            lista_studenata[0].prezime, lista_studenata[0].broj_indeksa)
        najbolji = lista_studenata[0]
        for i in range(1, len(lista_studenata)):
            if najbolji.prosecna_ocena < lista_studenata[i].prosecna_ocena:
                najbolji = lista_studenata[i]
        return "Najbolji je: {} {} sa indeksom: {}".format(najbolji.ime, najbolji.prezime, najbolji.broj_indeksa)