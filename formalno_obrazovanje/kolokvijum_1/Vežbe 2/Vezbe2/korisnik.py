from getpass import getpass


class Korisnik:
    def __init__(self, korisnicko_ime, lozinka):
        self.korisnicko_ime = korisnicko_ime
        self.lozinka = lozinka

    def uloguj_se(self):
        unos_korisnicko_ime = input("Unesite korisnicko ime: ")
        unos_lozinka = getpass("Unesite lozinku: ")
        if unos_korisnicko_ime == self.korisnicko_ime and unos_lozinka == self.lozinka:
            print("Uspesno logovanje!")
            return True
        else:
            print("Netacni podaci!")
            return False
