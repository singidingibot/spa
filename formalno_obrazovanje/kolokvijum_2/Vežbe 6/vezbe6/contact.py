class Contact:
    def __init__(self, first_name, last_name, phone_number):
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number

    def __eq__(self, other):
        if type(other) != Contact:
            raise TypeError("Nemoguce je porediti Contact i {}".format(type(other)))
        name = self.last_name + " " + self.first_name
        other_name = other.last_name + " " + other.first_name
        return name == other_name
    
    def __ge__(self, other):
        if type(other) != Contact:
            raise TypeError("Nemoguce je porediti Contact i {}".format(type(other)))
        name = self.last_name + " " + self.first_name
        other_name = other.last_name + " " + other.first_name
        return name >= other_name

    def __gt__(self, other):
        if type(other) != Contact:
            raise TypeError("Nemoguce je porediti Contact i {}".format(type(other)))
        name = self.last_name + " " + self.first_name
        other_name = other.last_name + " " + other.first_name
        return name > other_name

    def __str__(self):
        return "Contact >> first name: {}, last name: {}, phone number: {}".format(
            self.first_name, self.last_name, self.phone_number)