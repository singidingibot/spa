from random import randint
from sll.sll import SinglyLinkedList
from dll.dll import DoublyLinkedList
from contact import Contact

jednostruka = SinglyLinkedList()
dvostruka = DoublyLinkedList()

for i in range(10):
    jednostruka.append(i)
    dvostruka.append(i)


print("Broj 5 ima u listi?", jednostruka.sequential_search(5))
print("Broj 30 ima u listi?", dvostruka.sequential_search(30))

jednostruka.sort()
dvostruka.sort()
print("Broj 20 ima u listi?", dvostruka.binary_search(20))
print("Broj 4 ima u listi?", jednostruka.binary_search(4))

# Zadatak 1
prazna_jednostruka = SinglyLinkedList()
for i in range(10):
    prazna_jednostruka.append(randint(0, 100))

print("Broj 13 se nalazi u listi?", prazna_jednostruka.sequential_search(13))

# Zadatak 2
prazna_dvostruka = DoublyLinkedList()
for i in range(10):
    prazna_dvostruka.append(randint(0, 100))

prazna_dvostruka.sort()
print("Broj 5 se nalazi u listi?", prazna_dvostruka.binary_search(5))

# Zadatak 3
lista_kontakata = DoublyLinkedList()
marko_kontakt = Contact("Marko", "Markovic", "0601122333")
lista_kontakata.append(Contact("Marko", "Markovic", "0601122333"))
lista_kontakata.append(Contact("Janko", "Jankovic", "0611122333"))
lista_kontakata.append(Contact("Petar", "Petrovic", "0621122333"))
lista_kontakata.append(Contact("Milos", "Milosevic", "0631122333"))
lista_kontakata.append(Contact("Zdravko", "Zdravkovic", "0641122333"))
lista_kontakata.append(Contact("Ivan", "Ivanovic", "0651122333"))
lista_kontakata.append(Contact("Knez", "Knezevic", "0661122333"))
lista_kontakata.append(Contact("Zoran", "Zoric", "0691122333"))
lista_kontakata.append(Contact("Milan", "Milanovic", "0601122000"))

lista_kontakata.sort()

print("Marko Markovic se nalazi u kontaktima?", lista_kontakata.binary_search(marko_kontakt))