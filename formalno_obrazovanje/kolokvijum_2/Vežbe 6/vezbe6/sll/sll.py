from sll.node import Node

class SinglyLinkedList:
    """
    Klasa koja predstavlja jednostruko povezanu listu.
    """
    def __init__(self):
        """
        Inicijalizator jednostruko povezane liste.
        Lista ima pocetak (head), kraj (tail) i velicinu.
        """
        self.head = None # prazna lista nema pocetni cvor
        self.tail = None
        self.size = 0

    def append(self, value):
        """
        Metoda koja dodaje vrednost na kraj jednostruko povezane liste.
        :param value: vrednost koju dodajemo u listu, najpre se smesta u cvor
        """
        new_node = Node(value)
        # kada je lista prazna
        if self.size == 0:
            self.head = new_node
            self.tail = new_node
        # kada imamo samo jedan cvor
        elif self.head == self.tail: # ili if self.size == 1:
            self.head.pointer = new_node
            self.tail = new_node
        # kada imamo dva ili vise cvorova
        else:
            self.tail.pointer = new_node
            self.tail = new_node
        self.size += 1

    def prepend(self, value):
        """
        Metoda koja dodaje vrednost na pocetak jednostruko povezane liste.
        :param value: vrednost koju dodajemo u listu, najpre se smesta u cvor
        """
        new_node = Node(value)
        # kada je lista prazna
        if self.size == 0:
            self.head = new_node
            self.tail = new_node
        else:
            new_node.pointer = self.head
            self.head = new_node
        self.size += 1

    def remove_first(self):
        """
        Metoda koja uklanja cvor sa pocetka jednostruko povezane liste.
        """
        # ako je lista prazna
        value = None
        if self.size == 0:
            raise IndexError("Lista je prazna!")
        # ako ima samo jedan element
        elif self.head == self.tail:
            value = self.head.value
            self.head = None
            self.tail = None
        else:
            value = self.head.value
            self.head = self.head.pointer
        self.size -= 1
        return value

    def remove_last(self):
        """
        Metoda koja uklanja cvor sa kraja jednostruko povezane liste.
        """
        value = None
        # ako je lista prazna
        if self.size == 0:
            raise IndexError("Lista je prazna!")
        # ako ima samo jedan element
        elif self.head == self.tail:
            value = self.head.value
            self.head = None
            self.tail = None
        else:
            current = self.head
            while current.pointer != self.tail:
                current = current.pointer
            value = current.pointer.value
            current.pointer = None
            self.tail = current
        self.size -= 1
        return value

    def first(self):
        """
        Metoda koja dobavlja vrednost na pocetnom cvoru jednostruko povezane liste.
        """
        if self.size > 0:
            return self.head.value
        else:
            raise IndexError("Lista je prazna!")

    def last(self):
        """
        Metoda koja dobavlja vrednost na kranjem cvoru jednostruko povezane liste.
        """
        if self.size > 0:
            return self.tail.value
        else:
            raise IndexError("Lista je prazna!")

    def __getitem__(self, key):
        """
        Magicna metoda koja omogucava indeksiranje strukture. Sada je moguce da dobavimo vrednost
        na prosledjenoj poziciji preko: lista[key], gde je key indeks (pozicija) elementa kojeg
        zelimo da dobavimo.
        :param key: kljuc za indeksiranje
        :type key: int
        :returns: vrednost na datom indeksu
        :raises: :class `TypeError`: ako prosledjeni kljuc nije tipa int
        :raises: :class `IndexError`: ako je indeks van opsega strukture
        """
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for element in self:
                if counter == key:
                    return element
                counter += 1
        else:
            raise IndexError()
    
    def __setitem__(self, key, value):
        """
        Magicna metoda koja omogucava promenu sadrzaja strukture na zadatom indeksu.
        Sada je moguce da koristimo sledeci zapis lista[key] = value
        :param key: kljuc za indeksiranje
        :type key: int
        :param value: nova vrednost koja se postavlja
        :returns: vrednost na datom indeksu
        :raises: :class `TypeError`: ako prosledjeni kljuc nije tipa int
        :raises: :class `IndexError`: ako je indeks van opsega strukture
        """
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for node in self._iter_node():
                if counter == key:
                    node.value = value
                    break
                counter += 1
        else:
            raise IndexError()

    def __iter__(self):
        """
        Iterator koji omogucava generisanje elemenat koji se nalaze u jednostruko
        povezanoj listi. Iterator nam je potreban ako zelimo da for petljom prolazimo
        kroz jednostruko povezanu listu.
        """
        current = self.head
        while current is not None:
            yield current.value
            current = current.pointer

    def _iter_node(self):
        """
        Privatna metoda (pocinje sa _) koja vraca iterator cvorova.
        """
        current = self.head
        while current is not None:
            yield current
            current = current.pointer

    def __len__(self):
        """
        Redefinisani operator koji vraca duzinu strukture.
        """
        return self.size

    def sort(self):
        """
        Sortiranje liste uz pomoc selection sort algoritma.
        """
        for i in range(len(self)-1):
            j_min = i
            for j in range(i+1, len(self)):
                if self[j] < self[j_min]:
                    j_min = j
            if i != j_min:
                # zameni ta dva elementa (vrednosti na cvoru)
                self[i], self[j_min] = self[j_min], self[i]

    def sequential_search(self, value):
        for element in self:
            if element == value:
                return True
        return False
    
    def binary_search(self, value):
        start = 0
        end = len(self)-1
        while start <= end:
            middle = (start + end) // 2
            if value < self[middle]:
                end = middle - 1
            elif value > self[middle]:
                start = middle + 1
            else:
                return True
        return False