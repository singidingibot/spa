class Node:
    def __init__(self, value, prev_p=None, next_p=None):
        """
        Inicijalizator cvora.
        :param value: vrednost koju smestamo u cvor
        :param prev_p: pokazivac na cvor prethodnika trenutnog cvora
        :param next_p: pokazivac na cvor sledbenika trenutnog cvora
        """
        self.value = value
        self.prev_p = prev_p
        self.next_p = next_p