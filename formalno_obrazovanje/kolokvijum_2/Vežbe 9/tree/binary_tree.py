from abc import abstractmethod
from tree import Tree


class BinaryTree(Tree):
    """
    Klasa koja predstavlja binarno stablo, odnosno specijalizaciju (n-arnog) stabla.
    """
    @abstractmethod
    def left(self, p):
        """
        Vraca poziciju cvora koji je levo dete od p. Ako nema levo dete vraca None.
        :param p: roditelj cije levo dete trazimo
        """
        raise NotImplementedError("Not implemented!")

    @abstractmethod
    def right(self, p):
        """
        Vraca poziciju cvora koji je desno dete od p. Ako nema desno dete vraca None.
        :param p: roditelj cije desno dete trazimo
        """
        raise NotImplementedError("Not implemented!")

    @abstractmethod
    def sibling(self, p):
        """
        Vraca poziciju rodjaka od pozicije p. Ako nema rodjaka vraca None.
        :param p: pozicija cvora cijeg rodjaka trazimo
        """
        raise NotImplementedError("Not implemented!")