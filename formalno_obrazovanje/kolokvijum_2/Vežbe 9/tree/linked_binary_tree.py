from binary_tree import BinaryTree
from linked_binary_tree_position import LinkedBinaryTreePosition as Position # novo ime (alijas) za klasu
from linked_binary_tree_node import LinkedBinaryTreeNode as Node


class LinkedBinaryTree(BinaryTree):
    """
    Povezano binarno stablo. Upravljamo cvorovima preko pozicija stabla.
    """
    def __init__(self):
        self.root_node = None
        self.size = 0

    def _make_position(self, node):
        """
        Vraca poziciju cvora u stablu.
        """
        if node is not None:
            return Position(self, node)

    def _validate_position(self, p):
        """
        Proverava da li je pozicija ispravna i vraca cvor na poziciji
        :raises TypeError: ako je za p prosledjeno nesto sto nije Position
        :raises ValueError: ako p ne pripada istom stablu
        """
        if type(p) != Position:
            raise TypeError("Wrong type for param p")
        if self is not p.container:
            raise ValueError("Wrong container!")
        if p.node.parent is p.node: # kada se cvor obrise i zameni svojim detetom
            raise ValueError("Position p is not valid!")
        return p.node
            
    def root(self):
        # FIXME: ako je stablo prazno vratice None
        return self._make_position(self.root_node)

    def is_root(self, p):
        # TODO: drugi nacin, proveriti da li cvor nema parenta
        return p == self._make_position(self.root_node)

    def parent(self, p):
        child = self._validate_position(p)
        if child is not None:
            return self._make_position(child.parent)

    def num_children(self, p):
        counter = 0
        if self.left(p) is not None:
            counter += 1
        if self.right(p) is not None:
            counter += 1
        return counter

    def children(self, p):
        parent = self._validate_position(p)
        left = self._make_position(parent.left)
        right = self._make_position(parent.right)
        if left is not None:
            yield left
        if right is not None:
            yield right

    def is_empty(self):
        return True if self.size == 0 else False

    def __len__(self):
        return self.size

    def left(self, p):
        parent = self._validate_position(p)
        if parent is not None:
            return self._make_position(parent.left)

    def right(self, p):
        parent = self._validate_position(p)
        if parent is not None:
            return self._make_position(parent.right)

    def sibling(self, p):
        child = self._validate_position(p)
        if child is not None:
            parent = child.parent
            parent_position = self._make_position(parent)
            if self.right(parent_position) == p:
                return self.left(parent_position)
            else:
                return self.right(parent_position)

    def add_root(self, e):
        """
        Dodaje korenski cvor sa elementom e u stablo
        :param e: element koji ce biti smesten u cvor
        :return: pozicija novododatog cvora
        """
        if self.root_node is not None:
            raise ValueError("Root already exists.")
        self.size = 1
        self.root_node = Node(e)  # kada dodajemo korenski cvor on nema ni parent-a, ni levo ni desno dete
        return self._make_position(self.root_node)

    def add_left(self, p, e):
        """
        Dodaje novi cvor sa elementom e kao levo dete cvora smestenog na poziciji p.
        :param p: pozicija cvora kojem dodajemo dete
        :param e: vrednost novog cvora
        :return: pozicija novododatog cvora
        :raise ValueError: ako cvor na poziciji p vec ima levo dete
        """
        node = self._validate_position(p)
        if node.left is not None:
            raise ValueError("Left child exists.")
        self.size += 1
        node.left = Node(e, node)
        return self._make_position(node.left)


    def add_right(self, p, e):
        """
        Dodaje novi cvor sa elementom e kao desno dete cvora smestenog na poziciji p.
        :param p: pozicija cvora kojem dodajemo dete
        :param e: vrednost novog cvora
        :return: pozicija novododatog cvora
        :raise ValueError: ako cvor na poziciji p vec ima desno dete
        """
        node = self._validate_position(p)
        if node.right is not None:
            raise ValueError("Right child exists.")
        self.size += 1
        node.right = Node(e, node)
        return self._make_position(node.right)

    def replace(self, p, e):
        """
        Menja vrednost elementa za cvor na poziciji p i vraca staru vrednost elementa.
        :param p: pozicija cvora ciju vrednost menjamo
        :param e: nova vrednost
        :return: stara vrednost na poziciji p
        """
        node = self._validate_position(p)
        old = node.element
        node.element = e
        return old

    def delete(self, p):
        """
        Brise cvor na poziciji p i menja ga njegovim detetom, ako ga ima.
        :param p: pozicija cvora koji brisemo
        :return: vrednost elementa koji je bio na poziciji p
        :raise ValueError: ako cvor na poziciji p ima dva deteta
        """
        # I ove metode se mogu ouhvatiti obradom izuzetaka kao i prethodne
        node = self._validate_position(p)
        if self.num_children(p) == 2:
            raise ValueError("P has two children.")
        child = node.left if node.left else node.right
        if child is not None:
            child.parent = node.parent
        if node is self.root_node:
            self.root_node = child
        else:
            parent = node.parent
            if node is parent.left:
                parent.left = child
            else:
                parent.right = child
        self.size -= 1
        node.parent = node
        return node.element
    
    def attach(self, p, t1, t2):
        """
        Dodaje stabla t1 i t2 kao levo i desno podstablo pozicije p.
        :param p: pozicija gde dodajemo stabla
        :param t1: podstablo
        :param t2: podstablo
        :raise ValueError: ako cvor na poziciji p nije lisni cvor
        :raise TypeError: ako t1 i t2 nisu stabla
        """
        node = self._validate_position(p)
        if not self.is_leaf(p):
            raise ValueError("Position p must be a leaf.")
        if not type(self) is type(t1) is type(t2):
            raise TypeError("Tree types must match.")
        self.size += len(t1) + len(t2)  # sabiramo velicine stabala
        if not t1.is_empty():
            t1.root_node.parent = node
            node.left = t1.root_node
            t1.root_node = None
            t1.size = 0
        if not t2.is_empty():
            t2.root_node.parent = node
            node.right = t2.root_node
            t2.root_node = None
            t2.size = 0
        
