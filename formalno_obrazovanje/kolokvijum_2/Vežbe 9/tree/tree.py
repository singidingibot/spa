from abc import ABC, abstractmethod


class Tree(ABC):
    """
    Apstraktna klasa koja predstavlja bilo koje (n-arno) stablo.
    """
    @abstractmethod
    def root(self):
        """
        Vraca poziciju cvora koji je koren stabla
        """
        raise NotImplementedError("Not implemented!")

    @abstractmethod
    def is_root(self, p):
        """
        Vraca True ukoliko je p pozicija cvora koji je koren, u suprotnom False
        :param p: pozicija za koju proveravamo da li je koren
        """
        raise NotImplementedError("Not implemented!")

    @abstractmethod
    def parent(self, p):
        """
        Vraca poziciju cvora koji je roditelj od p.
        :param p: dete cijeg roditelja trazimo
        """
        raise NotImplementedError("Not implemented!")

    @abstractmethod
    def num_children(self, p):
        """
        Vraca broj dece od cvora na poziciji p.
        :param p: roditelj ciju decu prebrojavamo
        """
        raise NotImplementedError("Not implemented!")

    @abstractmethod
    def children(self, p):
        """
        Prolazi kroz svu decu i vraca njihove pozicije.
        :param p: roditelj na poziciji p ciju decu obilazimo
        """
        raise NotImplementedError("Not implemented!")

    def is_leaf(self, p):
        """
        Proverava da li je cvor na poziciji p list ili ne.
        :param p: pozicija za koju proveravamo da li je list
        """
        if self.num_children(p) == 0:
            return True
        else:
            return False

    def __len__(self):
        raise NotImplementedError("Not implemented!")

    @abstractmethod
    def is_empty(self):
        raise NotImplementedError("Not implemented!")