from position import Position


class LinkedBinaryTreePosition(Position):
    def __init__(self, container, node):
        self.container = container
        self.node = node

    def element(self):
        return self.node.element

    def __eq__(self, other):
        if type(self) == type(other) and self.node is other.node:
            return True
        else:
            return False