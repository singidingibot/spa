from abc import ABC, abstractmethod


class Position(ABC):
    @abstractmethod
    def element(self):
        raise NotImplementedError("Not implemented!")

    def __eq__(self, other):
        raise NotImplementedError("Not implemented!")

    def __ne__(self, other):
        return not self == other
