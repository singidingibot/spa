from sll.sll import SinglyLinkedList


class Stack:
    def __init__(self):
        """
        Realizacija steka preko dvostruko povezane liste.
        """
        self.lista = SinglyLinkedList()

    def push(self, value):
        """
        Metoda koja dodaje element na vrh steka.
        :param value: vrednost koja se dodaje
        """
        # ako kažemo da je vrh steka kraj strukture
        self.lista.append(value)

    def pop(self):
        """
        Metoda koja uklanja element sa vrha steka.
        :returns: vrednost sa vrha
        :raises IndexError: u slucaju da je struktura prazna.
        """
        try:
            return self.lista.remove_last()
        except IndexError as error:
            raise IndexError("Stek je prazan!")

    def top(self):
        """
        Metoda koja vraca element sa vrha steka.
        :returns: vrednost sa vrha
        :raises IndexError: u slucaju da je struktura prazna.
        """
        try:
            return self.lista.last()
        except IndexError as error:
            raise IndexError("Stek je prazan!")

    def __len__(self):
        """
        Redefinisana metoda koja vraca duzinu strukture.
        :returns: broj elemenata
        """
        return len(self.lista)

    