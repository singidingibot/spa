from linked_binary_tree import LinkedBinaryTree
from queue.queue import Queue
from stack.stack import Stack


class BinarySearchTree(LinkedBinaryTree):
    """
    Binarno stablo pretrage predstavlja sortirano binarno stablo.
    """
    def __init__(self):
        super().__init__()

    def insert_element(self, e):
        """
        Dodaje element e kao novi cvor u stablo. Cvor se dodaje na odgovarajucu poziciju spram
        vrednosti e koju zelimo da dodamo.
        :param e: element koji ce biti u novom cvoru
        :return:
        """
        if self.root() is None:
            self.add_root(e)
        else:
            self.insert_node(self.root(), e)

    def insert_node(self, root_p, value):
        """
        Dodaje cvor sa vrednoscu value na odgovarajucu poziciju u stablu.
        :param root_p: pocetna pozicija za pretragu pozicije na koju smestamo novi cvor
        :param value: vrednost koja ce biti umetnuta u novi cvor
        :return:
        """
        if value <= root_p.element():
            if root_p.node.left:
                # ako postoji levo dete od pozicije root_p, moramo se spustiti u podstablo sa korenom root_p
                self.insert_node(self.left(root_p), value)
            else:
                # ako ne postoji taj element mozemo dodati na levu poziciju
                self.add_left(root_p, value)
        elif value > root_p.element():
            if root_p.node.right:
                # ako postoji desno dete od pozicije root_p, moramo se spustiti u podstablo sa korenom root_p
                self.insert_node(self.right(root_p), value)
            else:
                # ako ne postoji taj element mozemo dodati na desnu poziciju
                self.add_right(root_p, value)

    # TODO: dodati algoritme prolazaka i pretrage stabala (vezbe u XV nedelji)
    def inorder(self):
        """
        Metoda koja generise iteracije inorder algoritma u stablu.
        :return:
        """
        if not self.is_empty():
            for p in self._subtree_inorder(self.root()):
                yield p

    def _subtree_inorder(self, p):
        """
        Metoda koja generise iteracije inorder algoritma u podstablu ciji je koren p.
        :param p: koren podstabla
        :return:
        """
        if self.left(p) is not None:
            for other in self._subtree_inorder(self.left(p)):
                yield other
        yield p
        if self.right(p) is not None:
            for other in self._subtree_inorder(self.right(p)):
                yield other

    def preorder(self):
        """
        Metoda koja generise iteracije preorder algoritma u stablu.
        :return:
        """
        if not self.is_empty():
            for p in self._subtree_preorder(self.root()):
                yield p

    def _subtree_preorder(self, p):
        """
        Metoda koja generise iteracije preorder algoritma u podstablu ciji je koren p.
        :param p: koren podstabla
        :return:
        """
        yield p
        for c in self.children(p):
            for other in self._subtree_preorder(c):
                yield other

    def positions(self):
        """
        Vraca generator pozicija stabla. Mozemo izabrati neki od navedenih metoda za prolaske.
        :return:
        """
        return self.preorder()
        # return self.inorder()
        # return self.postorder()

    def postorder(self):
        """
        Metoda koja generise iteracije postorder algoritma u stablu.
        :return:
        """
        if not self.is_empty():
            for p in self._subtree_postorder(self.root()):
                yield p

    def _subtree_postorder(self, p):
        """
        Metoda koja generise iteracije postorder algoritma u podstablu ciji je koren p.
        :param p: koren podstabla
        :return:
        """
        for c in self.children(p):
            for other in self._subtree_postorder(c):
                yield other
        yield p

    def breath_first(self):
        """
        Metoda koja generise iteracije breath-first algoritmom
        :return:
        """
        # TODO: dodati proveru da li je trenutni element trazeni (SEACH)
        # dodati i odgovarajuci parametar
        if not self.is_empty():
            fringe = Queue()  # obod
            fringe.enqueue(self.root())
            while not fringe.is_empty():
                p = fringe.dequeue()
                yield p
                for c in self.children(p):
                    fringe.enqueue(c)

    def depth_first(self):
        """
        Metoda koja generise iteracije depth-first algoritmom
        :return:
        """
        # TODO: dodati proveru da li je trenutni element trazeni (SEACH)
        # dodati i odgovarajuci parametar
        if not self.is_empty():
            s = Stack()
            s.push(self.root())
            while not s.is_empty():
                p = s.pop()
                yield p
                for c in self.children(p):
                    s.push(c)

    def binary_search(self, e, p):
        """
        Binarna pretraga nad stablom (rekurzivno).
        :param e: element koji trazimo u stablu
        :param p: pozicija od koje krecemo binarnu pretragu
        :return: True ako je pronadjen, u suprotnom False
        """
        # Uslov za prekid rekurzije
        if p is None:
            return False
        if p.element() == e:
            return True
        if e < p.element():
            # pretrazi levo podstablo
            return self.binary_search(e, self.left(p))
        else:
            # pretrazi desno podstablo
            return self.binary_search(e, self.right(p))