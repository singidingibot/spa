from binary_search_tree import BinarySearchTree


tree = BinarySearchTree()

tree.insert_element(15)
tree.insert_element(10)
tree.insert_element(5)
tree.insert_element(13)
tree.insert_element(28)
tree.insert_element(20)
tree.insert_element(30)


# Prikaz stabla
#                  15
#                /     \
#               10      28
#             /  \     /  \
#             5  13   20  30

# TODO: za proveru drugog algoritma, promeniti poziv u metodi positions
for p in tree.positions():
    print(p.node.element)

tree.breath_first()
tree.depth_first()

print(tree.binary_search(13, tree.root()))
print(tree.binary_search(33, tree.root()))