from sll.sll import SinglyLinkedList
from dll.dll import DoublyLinkedList


lista = SinglyLinkedList()
# lista.append(2)
# lista.append(5)
# lista.append(44)
# lista.append(23)
# lista.append(7)
# lista.append(11)
# lista.append(1)
# lista.append(3)
for i in range(5, -1, -1):
    lista.append(i)


print(lista)
print("Sortiranje")
lista.sort("bubble")
print("Nakon sortiranja:")
print(lista)