from dll.node import Node
# https://docs.python.org/3.8/reference/datamodel.html#emulating-container-types

class DoublyLinkedList:
    """
    Klasa koja predstavlja dvostruko povezanu listu.
    """
    def __init__(self):
        """
        Inicijalizator dvostruko povezane liste, koja je inicijalno prazna.
        """
        self.head = Node(None) # sentinel cvor pocetka
        self.tail = Node(None) # sentinel cvor kraja

        # uvezivanje cvorova
        self.head.next_p = self.tail
        self.tail.prev_p = self.head

        self.size = 0 # definisanje velicine liste da je 0

    def append(self, value):
        """
        Metoda koja dodaje vrednost value u cvor koji se smesta na kraj strukture.
        :param value: vrednost koju zelimo da dodamo na kraj
        """
        new_node = Node(value, self.tail.prev_p, self.tail)

        self.tail.prev_p.next_p = new_node
        self.tail.prev_p = new_node

        self.size += 1

    def prepend(self, value):
        """
        Metoda koja dodaje vrednost value u cvor koji se smesta na pocetak strukture.
        :param value: vrednost koju zelimo da dodamo na pocetak
        """
        new_node = Node(value, self.head, self.head.next_p)

        self.head.next_p.prev_p = new_node
        self.head.next_p = new_node

        self.size += 1

    def remove_first(self):
        """
        Metoda koja uklanja cvor sa pocetka strukture.
        """
        if self.size == 0:
            raise IndexError("Lista je prazna!")
        value = self.head.next_p.value
        self.head.next_p = self.head.next_p.next_p
        self.head.next_p.prev_p = self.head
        self.size -= 1
        return value

    def remove_last(self):
        """
        Metoda koja uklanja cvor sa kraja strukture.
        """
        if self.size == 0:
            raise IndexError("Lista je prazna!")
        value = self.tail.prev_p.value
        self.tail.prev_p.prev_p.next_p = self.tail
        self.tail.prev_p = self.tail.prev_p.prev_p

        self.size -= 1
        return value # moze biti None

    def first(self):
        """
        Metoda koja vraca vrednost smestenu u prvi cvor strukture.
        :return: vrednost prvog elementa
        """
        if self.size == 0:
            raise IndexError("Lista je prazna!")
        return self.head.next_p.value

    def last(self):
        """
        Metoda koja vraca vrednost smestenu u poslednji cvor strukture.
        :returns: vrednost poslednjeg elementa
        """
        if self.size == 0:
            raise IndexError("Lista je prazna!")
        return self.tail.prev_p.value

    def __iter__(self):
        """
        Magicna metoda koja vraca iterator vrednosti na cvorovima.
        """
        current = self.head.next_p
        while current != self.tail:
            yield current.value
            current = current.next_p

    def _iter_node(self):
        """
        Privatna metoda (pocinje sa _) koja vraca iterator cvorova.
        """
        current = self.head.next_p
        while current != self.tail:
            yield current
            current = current.next_p

    def __len__(self):
        """
        Magicna metoda koja redefinise ponasanje ugradjene funkcije len, nad ovom strukturom.
        """
        return self.size

    def __str__(self):
        """
        Magicna metoda koja vraca reprezentaciju objekta kao stringa.
        Vracena reprezentacija je bas ona poput klasicne Python liste: [1, 2, 3], gde su 1,2,3 vrednosti na
        cvorovima nase dvostruko povezane liste.
        """
        ret_val = "["
        for i in range(len(self)):
            ret_val += str(self[i])
            if i < len(self) - 1:
                ret_val += ", "
        return ret_val + "]"

    def __getitem__(self, key):
        """
        Magicna metoda koja omogucava indeksiranje strukture. Sada je moguce da dobavimo vrednost
        na prosledjenoj poziciji preko: lista[key], gde je key indeks (pozicija) elementa kojeg
        zelimo da dobavimo.
        :param key: kljuc za indeksiranje
        :type key: int
        :returns: vrednost na datom indeksu
        :raises: :class `TypeError`: ako prosledjeni kljuc nije tipa int
        :raises: :class `IndexError`: ako je indeks van opsega strukture
        """
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for element in self:
                if counter == key:
                    return element
                counter += 1
        else:
            raise IndexError()
    
    def __setitem__(self, key, value):
        """
        Magicna metoda koja omogucava promenu sadrzaja strukture na zadatom indeksu.
        Sada je moguce da koristimo sledeci zapis lista[key] = value
        :param key: kljuc za indeksiranje
        :type key: int
        :param value: nova vrednost koja se postavlja
        :returns: vrednost na datom indeksu
        :raises: :class `TypeError`: ako prosledjeni kljuc nije tipa int
        :raises: :class `IndexError`: ako je indeks van opsega strukture
        """
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for node in self._iter_node():
                if counter == key:
                    node.value = value
                    break
                counter += 1
        else:
            raise IndexError()

    def sort(self, method="selection"):
        """
        Metoda koja sortira listu. Sortiranje vrsi zamenom vrednosti na cvorovima strukture.
        Korisnik moze birati metod sortiranja. Tipove sortiranja i funkcije dobijamo iz sorting_algorithms.
        :param method: algoritam sortiranja (selection, bubble, bubble2, bubble2_optimized, insertion)
        """
        sort_function = self._sorting_algorithms.get(method, self.selection_sort)
        sort_function()

    def selection_sort(self):
        """
        Sortiranje liste uz pomoc selection sort algoritma.
        """
        for i in range(len(self)-1):
            j_min = i
            for j in range(i+1, len(self)):
                if self[j] < self[j_min]:
                    j_min = j
            if i != j_min:
                # zameni ta dva elementa (vrednosti na cvoru)
                self[i], self[j_min] = self[j_min], self[i]

    def bubble_sort(self): # sa slajdova
        """
        Sortiranje liste uz pomoc bubble sort algoritma.
        """
        for i in range(len(self)-1):
            for j in range(len(self)-1, i, -1):
                if self[j] < self[j-1]:
                    self[j], self[j-1] = self[j-1], self[j]
    
    def bubble_sort_wiki(self):
        """
        Sortiranje liste uz pomoc bubble sort algoritma.
        """
        # na osnovu gifa sa wikipedije (prvo sortiraj najveci)
        # proveravaju se elementi od pocetka strukture
        for i in range(len(self)):
            for j in range(0, len(self)-1-i):
                # desni deo ce nam biti i puta sortiran, pa ne proveravamo vise sortirani deo, zato idemo do 
                # duzina - i (a moramo i jos oduzeti 1, zato sto indeksiramo j + 1, da ne bi izasli iz opsega)
                if self[j] > self[j+1]: # kada krecemo poredjenje od pocetka, onda uzimamo po prva dva elementa
                    # zato ovde indeksiramo sa j i j+1
                    self[j], self[j+1] = self[j+1], self[j]

    def bubble_sort_2(self):
        """
        Sortiranje liste uz pomoc bubble sort algoritma.
        """
        s = True
        while s:
            s = False
            for j in range(len(self)-1, 0, -1):
                if self[j] < self[j-1]:
                    self[j], self[j-1] = self[j-1], self[j]
                    s = True

    def bubble_sort_2_optimized(self):
        """
        Sortiranje liste uz pomoc bubble sort algoritma (optimizovano).
        """
        s = True
        while s:
            s = False
            i = 0
            for j in range(len(self)-1, i, -1):
                if self[j] < self[j-1]:
                    self[j], self[j-1] = self[j-1], self[j]
                    s = True
                i += 1

    def insetion_sort(self):
        """
        Sortiranje liste uz pomoc insertion sort algoritma.
        """
        for i in range(len(self)):
            j = i
            while (j > 0) and (self[j] < self[j-1]):
                self[j], self[j-1] = self[j-1], self[j]
                print(self, "menjamo:", self[j], self[j-1])
                j = j-1

    def sequential_search(self, value):
        for element in self:
            if element == value:
                return True
        return False

    def binary_search(self, value):
        start = 0
        end = len(self)-1
        while start <= end:
            middle = (start + end) // 2
            if value < self[middle]:
                end = middle - 1
            elif value > self[middle]:
                start = middle + 1
            else:
                return True
        return False
