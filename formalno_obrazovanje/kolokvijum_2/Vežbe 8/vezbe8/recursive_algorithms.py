# TODO: Za domaci dodati ove algoritme u jednostruko i dvostruko povezanu listu
# TODO: Prilagoditi metodama koje poseduju te liste.
def list_split_from_book(l):
    l1 = []
    l2 = []

    d = True
    while len(l) != 0:
        x = l.pop(0)
        if d:
            l1.append(x)
        else:
            l2.append(x)
        d = not d
    return l1, l2

def list_split(l):
    mid = len(l)//2
    l1 = l[0:mid]
    l2 = l[mid:len(l)]

    return l1, l2

def merge(s1, s2, s):
    """
    Spaja sortirane sekvence s1 i s2 u rezultujucu sekvencu s.
    :param s1: sortirana leva polovna liste s
    :param s2: sortirana desna polovina liste s
    :param s: lista koju cemo reorganizovati spram prethodno sortiranih polovina
    :return:
    """
    i = j = k = 0  # indeksi za kretanje po sekvencama s1, s2 i s
    # za s1 koristimo i, za s2 koristimo j, i za s koristimo k
    while i < len(s1) and j < len(s2):  # ukoliko nismo prosli bar jednu od listi
        if s1[i] < s2[j]:  # uporedi trenutnim pozicijama listi s1 i s2 koji je manji element
            s[k] = s1[i]  # dodeli manji element u listu s
            i += 1 # inkrementiraj brojac i, kako se dodati broj vise ne bi poredio sa drugim elementima
        else:  # ukoliko je veci
            s[k] = s2[j]  # dodeli veci element u listu s
            j += 1  # inkrementiraj brojac j
        k += 1  # inkrementiraj brojac k kojim označavamo da smo popunili odgovarajući element liste

    while i < len(s1):  # ukoliko se desi da nismo preuzeli sve elemente iz liste
        # ovo se moze dogoditi kada neka od listi ima vise elemenata (npr. prilikom podele na dve liste
        # koja ima 11 elemenata, u jednoj ce biti 5, a u drugoj 6
        s[k] = s1[i]  # dodati ih na kraj liste s
        i += 1  # inkrementirati brojace i i k
        k += 1

    while j < len(s2):  # isto kao i za prethodnu while petlju
        s[k] = s2[j]
        j += 1
        k += 1

def merge_sort(s):
    if len(s) > 1: # ako nije trivijalni problem (jedan element u listi ili 0 elemenata)
        # podeli
        s1, s2 = list_split(s)
        # zavladaj
        merge_sort(s1)
        merge_sort(s2)
        # kombinuj
        merge(s1, s2, s)
    else:
        return


def quick_sort(s):
    """
    Metod sortiranja koji koristi rekurziju.
    Na osnovu pivot elementa deli strukturu s u 3 liste:
        l - struktura gde ce se smestiti svi elementi iz s koji su manji od pivot elementa
        e - struktura gde ce se smestiti svi elementi iz s koji su jednaki pivot elementu
        g - struktura gde ce se smestiti svi elementi iz s koji su veci od pivot elementa
    :param s: struktura koju sortiramo
    :return:
    """
    n = len(s)  # dobavljamo podatak o duzini strukture
    if n < 2: # ako lista ima 1 element ona je vec sortirana
        return  # lista je vec sortirana
    pivot = s[0]  # uzimamo prvi element liste za pivot element
    l = []  # lista koja ce biti popunjena elementima koji su manji od pivota
    e = []  # lista koja ce biti popunjena elementima koji su jednaki pivotu
    g = []  # lista koja ce biti popunjena elementima koji su veci od pivota
    while not (len(s) == 0):  # sve dok lista s nije prazna
        # podeli
        if s[0] < pivot:  # provera da li element treba da ide u listu l
            l.append(s.pop(0))  # uklanjamo element iz liste i dodajemo ga u listu l
            # TODO ukoliko se ne isprazne elementi iz liste, u nesortiranu listu ce se kasnije dodavati njeni sortirani delovi
            # TODO posto to ne zelimo, mi pomocu pop metode uklonimo element
            # TODO da smo samo pristupali elementima preko indeksa i na kraju prepisali listu s praznom listom
            # TODO izgubili bismo referencu na staru listu, i onda ne bi smo mogli da je koristimo za kombinovanje sortiranih vrednosti
        elif pivot < s[0]:  # provera da li element treba da ide u listu g
            g.append(s.pop(0))  # dodavanje u listu g
        else:  # ukoliko nije striktno manji i striktno veci onda je jednak pivot elementu
            e.append(s.pop(0))  # dodavanje uklonjenog elementa u listu e
    # zavladaj
    quick_sort(l)  # rekurzivno sortiramo manje liste
    quick_sort(g)
    # kombinuj
    while not (len(l) == 0):  # sve dok ne ispraznimo listu elemenata manjih od pivota
        s.append(l.pop(0))  # dodajemo ih redom u pocetnu listu, koja ce biti nas rezultat
    while not (len(e) == 0):  # sve dok ne ispraznimo listu elemenata koji su jednaki pivot elementu
        s.append(e.pop(0))  # dodajemo ih redom u pocetnu listu, u kojoj vec imamo sve elemente manje od pivota
    while not (len(g) == 0):  # sve dok ne ispraznimo listu elemenata koji su veci od pivot elementa
        s.append(g.pop(0))  # dodajemo ih redom u pocetnu listu, u kojoj vec imamo sve elemente koji su i manji i jednaki pivot elementu

l = []

import random
for i in range(10):
    l.append(random.randint(0, 100))

print(l)
merge_sort(l)
print(l)