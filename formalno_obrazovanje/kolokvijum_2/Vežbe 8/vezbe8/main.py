from dll.dll import DoublyLinkedList
from sll.sll import SinglyLinkedList

# lista = DoublyLinkedList()
# lista.append(20)
# lista.append(10)
# lista.append(5)
# lista.append(15)
# lista.append(0)

# # lista.quick_sort()
# lista.merge_sort()

# for element in lista:
#     print(element, end=" ")

lista = SinglyLinkedList()
lista.append(20)
lista.append(10)
lista.append(5)
lista.append(15)
lista.append(0)

lista.merge_sort()
for element in lista:
    print(element)