from sll.node import Node

class SinglyLinkedList:
    """
    Klasa koja predstavlja jednostruko povezanu listu.
    """
    def __init__(self):
        """
        Inicijalizator jednostruko povezane liste.
        Lista ima pocetak (head), kraj (tail) i velicinu.
        """
        self.head = None # prazna lista nema pocetni cvor
        self.tail = None
        self.size = 0
        self._sorting_algorithms = {
            "selection": self.selection_sort,
            "bubble": self.bubble_sort,
            "bubble_wiki": self.bubble_sort_wiki,
            "bubble2": self.bubble_sort_2,
            "bubble2_optimized": self.bubble_sort_2_optimized 
        }

    def append(self, value):
        """
        Metoda koja dodaje vrednost na kraj jednostruko povezane liste.
        :param value: vrednost koju dodajemo u listu, najpre se smesta u cvor
        """
        new_node = Node(value)
        # kada je lista prazna
        if self.size == 0:
            self.head = new_node
            self.tail = new_node
        # kada imamo samo jedan cvor
        elif self.head == self.tail: # ili if self.size == 1:
            self.head.pointer = new_node
            self.tail = new_node
        # kada imamo dva ili vise cvorova
        else:
            self.tail.pointer = new_node
            self.tail = new_node
        self.size += 1

    def prepend(self, value):
        """
        Metoda koja dodaje vrednost na pocetak jednostruko povezane liste.
        :param value: vrednost koju dodajemo u listu, najpre se smesta u cvor
        """
        new_node = Node(value)
        # kada je lista prazna
        if self.size == 0:
            self.head = new_node
            self.tail = new_node
        else:
            new_node.pointer = self.head
            self.head = new_node
        self.size += 1

    def remove_first(self):
        """
        Metoda koja uklanja cvor sa pocetka jednostruko povezane liste.
        """
        # ako je lista prazna
        value = None
        if self.size == 0:
            raise IndexError("Lista je prazna!")
        # ako ima samo jedan element
        elif self.head == self.tail:
            value = self.head.value
            self.head = None
            self.tail = None
        else:
            value = self.head.value
            self.head = self.head.pointer
        self.size -= 1
        return value

    def remove_last(self):
        """
        Metoda koja uklanja cvor sa kraja jednostruko povezane liste.
        """
        value = None
        # ako je lista prazna
        if self.size == 0:
            raise IndexError("Lista je prazna!")
        # ako ima samo jedan element
        elif self.head == self.tail:
            value = self.head.value
            self.head = None
            self.tail = None
        else:
            current = self.head
            while current.pointer != self.tail:
                current = current.pointer
            value = current.pointer.value
            current.pointer = None
            self.tail = current
        self.size -= 1
        return value

    def first(self):
        """
        Metoda koja dobavlja vrednost na pocetnom cvoru jednostruko povezane liste.
        """
        if self.size > 0:
            return self.head.value
        else:
            raise IndexError("Lista je prazna!")

    def last(self):
        """
        Metoda koja dobavlja vrednost na kranjem cvoru jednostruko povezane liste.
        """
        if self.size > 0:
            return self.tail.value
        else:
            raise IndexError("Lista je prazna!")

    def __getitem__(self, key):
        """
        Magicna metoda koja omogucava indeksiranje strukture. Sada je moguce da dobavimo vrednost
        na prosledjenoj poziciji preko: lista[key], gde je key indeks (pozicija) elementa kojeg
        zelimo da dobavimo.
        :param key: kljuc za indeksiranje
        :type key: int
        :returns: vrednost na datom indeksu
        :raises: :class `TypeError`: ako prosledjeni kljuc nije tipa int
        :raises: :class `IndexError`: ako je indeks van opsega strukture
        """
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for element in self:
                if counter == key:
                    return element
                counter += 1
        else:
            raise IndexError()
    
    def __setitem__(self, key, value):
        """
        Magicna metoda koja omogucava promenu sadrzaja strukture na zadatom indeksu.
        Sada je moguce da koristimo sledeci zapis lista[key] = value
        :param key: kljuc za indeksiranje
        :type key: int
        :param value: nova vrednost koja se postavlja
        :returns: vrednost na datom indeksu
        :raises: :class `TypeError`: ako prosledjeni kljuc nije tipa int
        :raises: :class `IndexError`: ako je indeks van opsega strukture
        """
        if type(key) != int:
            raise TypeError()
        if 0 <= key < self.size:
            counter = 0
            for node in self._iter_node():
                if counter == key:
                    node.value = value
                    break
                counter += 1
        else:
            raise IndexError()

    def __str__(self):
        """
        Magicna metoda koja vraca reprezentaciju objekta kao stringa.
        Vracena reprezentacija je bas ona poput klasicne Python liste: [1, 2, 3], gde su 1,2,3 vrednosti na
        cvorovima nase dvostruko povezane liste.
        """
        ret_val = "["
        for i in range(len(self)):
            ret_val += str(self[i])
            if i < len(self) - 1:
                ret_val += ", "
        return ret_val + "]"

    def __iter__(self):
        """
        Iterator koji omogucava generisanje elemenat koji se nalaze u jednostruko
        povezanoj listi. Iterator nam je potreban ako zelimo da for petljom prolazimo
        kroz jednostruko povezanu listu.
        """
        current = self.head
        while current is not None:
            yield current.value
            current = current.pointer

    def _iter_node(self):
        """
        Privatna metoda (pocinje sa _) koja vraca iterator cvorova.
        """
        current = self.head
        while current is not None:
            yield current
            current = current.pointer

    def __len__(self):
        """
        Redefinisani operator koji vraca duzinu strukture.
        """
        return self.size

    def sort(self, method="selection"):
        """
        Metoda koja sortira listu. Sortiranje vrsi zamenom vrednosti na cvorovima strukture.
        Korisnik moze birati metod sortiranja. Tipove sortiranja i funkcije dobijamo iz sorting_algorithms.
        :param method: algoritam sortiranja (selection, bubble, bubble2, bubble2_optimized, insertion)
        """
        sort_function = self._sorting_algorithms.get(method, self.selection_sort)
        sort_function()

    def selection_sort(self):
        """
        Sortiranje liste uz pomoc selection sort algoritma.
        """
        for i in range(len(self)-1):
            j_min = i
            for j in range(i+1, len(self)):
                if self[j] < self[j_min]:
                    j_min = j
            if i != j_min:
                # zameni ta dva elementa (vrednosti na cvoru)
                self[i], self[j_min] = self[j_min], self[i]

    def bubble_sort(self): # sa slajdova
        """
        Sortiranje liste uz pomoc bubble sort algoritma.
        """
        for i in range(len(self)-1):
            for j in range(len(self)-1, i, -1):
                if self[j] < self[j-1]:
                    self[j], self[j-1] = self[j-1], self[j]
    
    def bubble_sort_wiki(self):
        """
        Sortiranje liste uz pomoc bubble sort algoritma.
        """
        # na osnovu gifa sa wikipedije (prvo sortiraj najveci)
        # proveravaju se elementi od pocetka strukture
        for i in range(len(self)):
            for j in range(0, len(self)-1-i):
                # desni deo ce nam biti i puta sortiran, pa ne proveravamo vise sortirani deo, zato idemo do 
                # duzina - i (a moramo i jos oduzeti 1, zato sto indeksiramo j + 1, da ne bi izasli iz opsega)
                if self[j] > self[j+1]: # kada krecemo poredjenje od pocetka, onda uzimamo po prva dva elementa
                    # zato ovde indeksiramo sa j i j+1
                    self[j], self[j+1] = self[j+1], self[j]

    def bubble_sort_2(self):
        """
        Sortiranje liste uz pomoc bubble sort algoritma.
        """
        s = True
        while s:
            s = False
            for j in range(len(self)-1, 0, -1):
                if self[j] < self[j-1]:
                    self[j], self[j-1] = self[j-1], self[j]
                    s = True

    def bubble_sort_2_optimized(self):
        """
        Sortiranje liste uz pomoc bubble sort algoritma (optimizovano).
        """
        s = True
        while s:
            s = False
            i = 0
            for j in range(len(self)-1, i, -1):
                if self[j] < self[j-1]:
                    self[j], self[j-1] = self[j-1], self[j]
                    s = True
                i += 1

    def insetion_sort(self):
        """
        Sortiranje liste uz pomoc insertion sort algoritma.
        """
        for i in range(len(self)):
            j = i
            while (j > 0) and (self[j] < self[j-1]):
                self[j], self[j-1] = self[j-1], self[j]
                print(self, "menjamo:", self[j], self[j-1])
                j = j-1
    
    def list_split(self):
        mid = len(self)//2
        l1 = SinglyLinkedList()
        l2 = SinglyLinkedList()
        for i in range(mid):
            l1.append(self[i])
        for i in range(mid, len(self)):
            l2.append(self[i])

        return l1, l2

    def merge(self, s1, s2):
        """
        Spaja sortirane sekvence s1 i s2 u rezultujucu sekvencu s.
        :param s1: sortirana leva polovna liste self
        :param s2: sortirana desna polovina liste self
        :return:
        """
        i = j = k = 0  # indeksi za kretanje po sekvencama s1, s2 i s
        # za s1 koristimo i, za s2 koristimo j, i za s koristimo k
        while i < len(s1) and j < len(s2):  # ukoliko nismo prosli bar jednu od listi
            if s1[i] < s2[j]:  # uporedi trenutnim pozicijama listi s1 i s2 koji je manji element
                self[k] = s1[i]  # dodeli manji element u listu s
                i += 1 # inkrementiraj brojac i, kako se dodati broj vise ne bi poredio sa drugim elementima
            else:  # ukoliko je veci
                self[k] = s2[j]  # dodeli veci element u listu s
                j += 1  # inkrementiraj brojac j
            k += 1  # inkrementiraj brojac k kojim označavamo da smo popunili odgovarajući element liste

        while i < len(s1):  # ukoliko se desi da nismo preuzeli sve elemente iz liste
            # ovo se moze dogoditi kada neka od listi ima vise elemenata (npr. prilikom podele na dve liste
            # koja ima 11 elemenata, u jednoj ce biti 5, a u drugoj 6
            self[k] = s1[i]  # dodati ih na kraj liste s
            i += 1  # inkrementirati brojace i i k
            k += 1

        while j < len(s2):  # isto kao i za prethodnu while petlju
            self[k] = s2[j]
            j += 1
            k += 1

    def merge_sort(self):
        if len(self) > 1: # ako nije trivijalni problem (jedan element u listi ili 0 elemenata)
            # podeli
            s1, s2 = self.list_split()
            # zavladaj
            s1.merge_sort()
            s2.merge_sort()
            # kombinuj
            self.merge(s1, s2)
        else:
            return   

    def sequential_search(self, value):
        for element in self:
            if element == value:
                return True
        return False
    
    def binary_search(self, value):
        start = 0
        end = len(self)-1
        while start <= end:
            middle = (start + end) // 2
            if value < self[middle]:
                end = middle - 1
            elif value > self[middle]:
                start = middle + 1
            else:
                return True
        return False